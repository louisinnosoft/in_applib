﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aras.IOM;
using System.Xml;
using System.Web;
using Innosoft;

using System.Management;

//2017-08-24 : in_company機制
/*公司別過濾效果:
	1.一般使用者搜尋的時候,一律補上公司別=自己 + 公司別為空值
	2.若屬於 AllCompany 群組,則不需過慮公司別
	3.若找不到 AllCompany,則不需要公司別過濾效果
*/

namespace Innosoft
{
    public class app
    {
        Innovator inn;

        string UserInfo = "";
        string lang = "";
        string PolyItems = "";
        public string AppVer = "1";
        XmlDocument xmlui_resource = new XmlDocument();
        InnovatorHelper _InnH = null;

        public app(Innovator thisinn, string thisUserInfo)
        {
            Init_app(thisinn, thisUserInfo);
        }

        public app(Innovator thisinn)
        {
            inn = thisinn;
            string thisUserInfo = "<UserInfo>";
            thisUserInfo += "<loginid>" + thisinn.getConnection().getUserID() + "</loginid>";
            thisUserInfo += "<db>" + thisinn.getConnection().GetDatabaseName() + "</db>";
            thisUserInfo += "<pwd>000</pwd>";
            thisUserInfo += "<clienttemplates>000</clienttemplates>";
            thisUserInfo += "<lang>" + thisinn.getI18NSessionContext().GetLanguageCode() + "</lang>";
            thisUserInfo += "</UserInfo>";


            Init_app(thisinn, thisUserInfo);
        }

        private void Init_app(Innovator thisinn, string thisUserInfo)
        {
            inn = thisinn;
           _InnH = new Innosoft.InnovatorHelper(inn);
            UserInfo = thisUserInfo;
            lang = GetUserInfo(UserInfo, "lang");
        }



        public string GetSalt()
        {
            string r = "";
            string aml = "<AML>";
            aml += "<Item type='Variable' action='get'>";
            aml += "<name>Inno.MetadataService.Salt</name>";
            aml += "</Item>";
            aml += "</AML>";

            Item Salt = inn.applyAML(aml);

            r = (Salt.isError()) ? "0" : Salt.getProperty("value", "0");
            return r;
        }

        public string GetParameterValue(string Parameter, string Key, char Deli1, char Deli2)
        {
            //System.Diagnostics.Debugger.Break();;;
            //itemtype:project&where:[state]='active'&orderby:created_on DESC&top:30&aml=<start>2012-12-22T00:00"00</start>
            //因为有可能是 2012-12-11T00:00:00, 因此不能单纯使用 : 去 split
            //从Key + Deli2 开始取,一路取到 下一个 Deli1 或字符串结尾
            //GetParameterValue(criteria, "itemtype", '&', ':');
            string r = GetParameterValue(Parameter, Key, Deli1, Deli2, "");
            return r;
        }

        public string GetParameterValue(string Parameter, string Key, char Deli1, char Deli2, string DefaultValue)
        {
            //System.Diagnostics.Debugger.Break();;;
            //itemtype:project&where:[state]='active'&orderby:created_on DESC&top:30&aml=<start>2012-12-22T00:00"00</start>
            //因为有可能是 2012-12-11T00:00:00, 因此不能单纯使用 : 去 split
            //从Key + Deli2 开始取,一路取到 下一个 Deli1 或字符串结尾
            //GetParameterValue(criteria, "itemtype", '&', ':');
            string r = DefaultValue;
            try
            {
                if (Parameter.IndexOf(Key + Deli2) >= 0)
                {
                    int ValueStartIndex = Parameter.IndexOf(Key + Deli2) + (Key.Length + 1);
                    int ValueEndIndex = Parameter.IndexOf(Deli1, ValueStartIndex);
                    ValueEndIndex = ValueEndIndex < 0 ? Parameter.Length : ValueEndIndex;

                    r = Parameter.Substring(ValueStartIndex, (ValueEndIndex - ValueStartIndex));
                    if (r == "")
                        r = DefaultValue;
                }

            }
            catch (Exception ex)
            {
                r = DefaultValue;
            }
            return r;
        }


        public string GetSearchResult(string criteria)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            //criteria : itemtype:project&where:[state]='active'&orderby:created_on DESC&top:30

            //string[] param = criteria.Split('&');
            //string itemtype = param[0].Split(':')[1];
            //string where = param[1].Split(':')[1];
            //string orderby = param[2].Split(':')[1];


            //string[] param = criteria.Split('&');
            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':');
            string where = GetParameterValue(criteria, "where", '&', ':');
            string orderby = GetParameterValue(criteria, "orderby", '&', ':');
            string top = GetParameterValue(criteria, "top", '&', ':');
            string page = GetParameterValue(criteria, "page", '&', ':');
            string pagesize = GetParameterValue(criteria, "pagesize", '&', ':');
            string c_aml = GetParameterValue(criteria, "aml", '&', ':');
            string loadmore = GetParameterValue(criteria, "loadmore", '&', ':', "false");

            string aml = "";
            string strPropertySet = "";
            Item item_PropertySet;
            //Item itmItemType = inn.getItemByKeyedName("ItemType", itemtype);

            GetPropertySet(itemtype, "in_web_list", out strPropertySet, out item_PropertySet);

            string str_r = "";

            str_r += "<Header>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {
                Item tmp = item_PropertySet.getItemByIndex(i);
                str_r += "<p" + tmp.getProperty("in_web_list") + "_label>";
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + tmp.getProperty("in_web_list") + "_label>";
            }
            str_r += "</Header>";

            strPropertySet = strPropertySet.Trim(',');

            if (strPropertySet == "")
            {
                //exception
            }

            str_r += "<Input>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {

                Item item_Property = item_PropertySet.getItemByIndex(i);
                string AppPropertyName = item_Property.getProperty("name");

                str_r += FormatValue(item_Property, item_Property, AppPropertyName, "p" + item_Property.getProperty("in_web_list") + "_input", "#empty");
            }
            str_r += "</Input>";

            //2017-08-24 : in_company機制
            //in_company機制
            /*公司別過濾效果:
	            1.一般使用者搜尋的時候,一律補上公司別=自己 + 公司別為空值
	            2.若屬於 AllCompany 群組,則不需過慮公司別
	            3.若找不到 AllCompany,則不需要公司別過濾效果
            */

            var IsAllCompany = "";
           
            var strMyCompany = "";

            if (IsAllCompany == "")
            {
                var d_aml = "<AML>";
                d_aml += "<Item type='Identity' action='get'>";
                d_aml += "<name>AllCompany</name>";
                d_aml += "</Item></AML>";

                Item itmAllCompany = inn.applyAML(d_aml);
                //itmAllCompany  = aras.getItemByName("identity","AllCompany",0);
                if (!itmAllCompany.isError())
                {
                    //var strAllCompanyId = aras.getItemProperty(itmAllCompany, 'id');								
                    var strAllCompanyId = itmAllCompany.getID();
                    //var strAllCompanyId = aras.getItemProperty(itmAllCompany, 'id');								
                    if (inn.applyMethod("In_GetIdentityList", "<user_id>" + inn.getConnection().getUserID() + "</user_id>").getResult().Contains(strAllCompanyId))
                    {
                        IsAllCompany = "true";
                    }
                    else
                    {
                        IsAllCompany = "false";
                        if (strMyCompany == "")
                        {
                            d_aml = "<AML>";
                            d_aml += "<Item type='User' action='get'>";
                            d_aml += "<id>" + inn.getConnection().getUserID() + "</id>";
                            d_aml += "</Item></AML>";
                            Item itmUser = inn.applyAML(d_aml);

                            strMyCompany = itmUser.getProperty("in_company", "");
                        }
                    }

                }
                else
                {
                    IsAllCompany = "true";
                }

            }

            string strAMLCompany = "";
            string strWhereCompany = "";

            if (IsAllCompany == "false")
            {
                if (strMyCompany != "")
                {

                    strAMLCompany = "<or><in_company>" + strMyCompany + "</in_company><in_company condition='is null'></in_company></or>";
                    strWhereCompany = " and (in_company='" + strMyCompany + "' or in_company is null)";
                }
            }

            if (c_aml != "")
            {
                string tmp = "";
                tmp = "<AML>";
                tmp += "<Item type='" + itemtype + "' ";
                tmp += " action='get' ";
                tmp += "select ='" + strPropertySet + "'  ";
                tmp += "maxRecords ='" + top + "' ";
                tmp += "orderBy ='" + orderby + "' ";
                if (pagesize != "")
                    tmp += "pagesize='" + pagesize + "' ";
                if (page != "")
                    tmp += "page='" + page + "'";
                tmp += ">";

                tmp += c_aml;
                tmp += strAMLCompany;
                tmp += "</Item>";
                tmp += "</AML>";
                c_aml = tmp;

                //System.Diagnostics.Debugger.Break();;;                
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(c_aml);
                //Searchall
                XmlNode SearchAllItem = xmldoc.SelectSingleNode("//searchall");
                if (SearchAllItem != null)
                {
                    string SearchAllCriteria = SearchAllItem.InnerText;

                    string[] SearchAllCriteriaArr = SearchAllCriteria.Split(' ');

                    string SearcallAML = "";
                    SearcallAML = "<or>";
                    for (int i = 0; i < item_PropertySet.getItemCount(); i++)
                    {
                        Item item_Property = item_PropertySet.getItemByIndex(i);
                        string AppPropertyName = item_Property.getProperty("name");
                        if(int.Parse(item_Property.getProperty("in_web_list","0"))>0)
                        if (item_Property.getProperty("data_type", "") == "item")
                        {
                            SearcallAML += "<" + AppPropertyName + " condition='in'>";
                            SearcallAML += "<Item type='" + item_Property.getProperty("data_source", "") + "' action='get'>";
                            SearcallAML += "<keyed_name condition='like'>*" + SearchAllCriteria + "*</keyed_name>";
                            SearcallAML += "</Item>";
                            SearcallAML += "</" + AppPropertyName + ">";
                        }
                        else
                        {
                            SearcallAML += "<" + AppPropertyName + " condition='like'>";
                            SearcallAML += "*" + SearchAllCriteria + "*";
                            SearcallAML += "</" + AppPropertyName + ">";
                        }
                    }
                    SearcallAML += "</or>";


                    XmlDocumentFragment xfrag = xmldoc.CreateDocumentFragment();
                    xfrag.InnerXml = SearcallAML;
                    xmldoc.DocumentElement.SelectSingleNode("//Item[@type='" + itemtype + "']").AppendChild(xfrag);

                    ////in-input-alias
                    //XmlNode AilasItem = xmldoc.SelectSingleNode("//alias");
                    //if (AilasItem != null)
                    //{
                    //    string AliasAML = "<is_alias>" + AilasItem.InnerText + "</is_alias>";
                    //    XmlDocumentFragment xfrag_alias = xmldoc.CreateDocumentFragment();
                    //    xfrag_alias.InnerXml = AliasAML;
                    //    xmldoc.DocumentElement.SelectSingleNode("//Item[@type='" + itemtype + "']").AppendChild(xfrag_alias);
                    //}

                }
                aml = xmldoc.OuterXml;
            }
            else
            {
                if (where != "")
                    where = "where=\"" + where + strWhereCompany +  "\"";
                else
                    where = "";

                aml = "<AML>";
                aml += "<Item type='" + itemtype + "' ";
                aml += where;
                aml += " action='get' ";
                aml += "select ='" + strPropertySet + "'  ";
                aml += "maxRecords ='" + top + "' ";
                aml += "orderBy ='" + orderby + "' ";
                if (pagesize != "")
                    aml += "pagesize='" + pagesize + "' ";
                if (page != "")
                    aml += "page='" + page + "' ";
                aml += ">";
                if (where == "")
                    aml += strAMLCompany;
                aml += "</Item>";
                aml += "</AML>";
            }

            Item item_Result = inn.applyAML(aml);
            if (item_Result.isError())
            {
                //exception
            }

            string strPageMax = ""; //总页数
            string strItemXax = ""; //总笔数
            for (int i = 0; i < item_Result.getItemCount(); i++)
            {
                str_r += "<Item>";
                Item tmp = item_Result.getItemByIndex(i);
                strPageMax = tmp.getAttribute("pagemax", "1");
                strItemXax = tmp.getAttribute("itemmax", "");
                str_r += "<itemid>" + tmp.getID() + "</itemid>";
                str_r += "<itemtype>" + itemtype + "</itemtype>";
                str_r += "<keyed_name>" + System.Security.SecurityElement.Escape(tmp.getPropertyAttribute("id", "keyed_name", "")) + "</keyed_name>";
                for (int j = 0; j < item_PropertySet.getItemCount(); j++)
                {
                    str_r += "\n";
                    Item item_Property = item_PropertySet.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    string strNodeName = "p" + item_Property.getProperty("in_web_list") + "_value";
                    if (AppVer == "2")
                        strNodeName = AppPropertyName;
                    str_r += FormatValue(tmp, item_Property, AppPropertyName, strNodeName, "");
                    str_r += "\n";
                }
                str_r += "</Item>\n";
            }

            //GetActionButton
            aml = "<AML>";
            aml += "<Item type='Item Action' action='get' select='in_web_list,related_id(type,label,body)' orderBy='in_web_list'>";
            aml += "<source_id>" + inn.getItemByKeyedName("ItemType", itemtype).getID() + "</source_id>";
            aml += "</Item>";
            aml += "</AML>";

            item_Result = inn.applyAML(aml);
            if (!item_Result.isError() && item_Result.getItemCount() > 0)
            {
                str_r += "<Actions>";
                for (int k = 0; k < item_Result.getItemCount(); k++)
                {
                    Item tmp_action = item_Result.getItemByIndex(k).getRelatedItem();

                    if (tmp_action.getProperty("type") == "app_itemtype")
                    {
                        string body = tmp_action.getProperty("body");
                        string url = getParameter(body, "url", ';');
                        //string name = tmp_action.getProperty("label", "", lang);
                        string name = GetMultilangProperty(tmp_action, "label");

                        url = url.Replace("{@itemtype}", itemtype);

                        str_r += "<action>";
                        str_r += "<url><![CDATA[" + url + "]]></url>";
                        str_r += "<label>" + name + "</label>";
                        str_r += "</action>";
                    }
                }
                str_r += "</Actions>";
            }

            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' select='name,related_id(name)'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>in_web_list</type>";
            aml += "</Item>";
            aml += "</AML>";

            item_Result = inn.applyAML(aml);
            string r_templatename = "in_default_web_list";
            if (item_Result.getItemCount() > 0)
            {
                r_templatename = item_Result.getItemByIndex(0).getRelatedItem().getProperty("name");
            }
            string Status = "true";

            aml = "<AML>";
            aml += "<Item type='ItemType' action='get' language='" + lang + "' select='label'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</AML>";

            Item item_r = inn.applyAML(aml);
            str_r += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";
            str_r += "<itemtype>" + itemtype + "</itemtype>";
            str_r += "<page>" + page + "</page>";
            str_r += "<pagemax>" + strPageMax + "</pagemax>";
            str_r += "<itemmax>" + strItemXax + "</itemmax>";
            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            criteria += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";
            criteria += "<loadmore>" + loadmore + "</loadmore>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);



            return r;

        }


        public string GetSearchForm(string criteria)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            //criteria : itemtype:project&where:[state]='active'&orderby:created_on DESC&top:30

            //string[] param = criteria.Split('&');
            //string itemtype = param[0].Split(':')[1];
            //string where = param[1].Split(':')[1];
            //string orderby = param[2].Split(':')[1];


            //string[] param = criteria.Split('&');
            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':');
            string orderby = GetParameterValue(criteria, "orderby", '&', ':');
            string top = GetParameterValue(criteria, "top", '&', ':');

            string aml = "";
            string strPropertySet = "";
            Item item_PropertySet;

            GetPropertySet(itemtype, "in_web_list", out strPropertySet, out item_PropertySet);

            string str_r = "";

            str_r += "<Header>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {
                Item tmp = item_PropertySet.getItemByIndex(i);
                str_r += "<p" + tmp.getProperty("in_web_list") + "_label>";
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + tmp.getProperty("in_web_list") + "_label>";
            }
            str_r += "</Header>";

            strPropertySet = strPropertySet.Trim(',');

            if (strPropertySet == "")
            {
                //exception
            }

            str_r += "<Input>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {
                Item item_Property = item_PropertySet.getItemByIndex(i);
                string AppPropertyName = item_Property.getProperty("name");                
                str_r += FormatValue(item_Property, item_Property, AppPropertyName, "p" + item_Property.getProperty("in_web_list") + "_input", "#empty");
            }
            str_r += "</Input>";


            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' select='name,related_id(name)'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>in_web_list</type>";
            aml += "</Item>";
            aml += "</AML>";

            Item item_Result = inn.applyAML(aml);
            string r_templatename = "in_default_web_list";
            if (item_Result.getItemCount() > 0)
            {
                r_templatename = item_Result.getItemByIndex(0).getRelatedItem().getProperty("name");
            }
            string Status = "true";
            str_r += "<itemtype>" + itemtype + "</itemtype>";

            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);



            return r;

        }

        public void GetPropertySet(string itemtypename, string app, out string strPropertySet, out Item itemPropertySets)
        {
            string aml = "";

            //System.Diagnostics.Debugger.Break();;;

            aml = "<AML>";
            //aml += "<Item type='Property' action='get' select='name,label,data_type,data_source,is_required,pattern," + app + "' orderBy='" + app + "'  language='" + GetUserInfo(UserInfo,"lang") + "'>";
            aml += "<Item type='Property' action='get'  orderBy='" + app + "'  language='" + GetUserInfo(UserInfo, "lang") + "'>";
            aml += "<source_id>";
            aml += "<Item type='Itemtype' action='get' select='id'>";
            aml += "<name>" + itemtypename + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            //aml += "<" + app + " condition='gt'>0</" + app + ">";
            aml += "</Item>";
            aml += "</AML>";

            itemPropertySets = inn.applyAML(aml);


            strPropertySet = "";

            for (int i = 0; i < itemPropertySets.getItemCount(); i++)
            {
                Item itemPropertySet = itemPropertySets.getItemByIndex(i);
                if (itemPropertySet.getProperty("name") == "id" || itemPropertySet.getProperty("name") == "config_id")
                    continue;
                if (int.Parse(itemPropertySet.getProperty(app, "0")) > 0 || itemPropertySet.getProperty("data_type", "") == "item")
                    strPropertySet += itemPropertySet.getProperty("name") + ",";
            }
            strPropertySet = strPropertySet.Trim(',');

            return;
        }

        public string GetMultilangProperty(Item Context, string PropertyName)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            r = Context.getProperty(PropertyName, "", GetUserInfo(UserInfo, "lang"));
            if (r == "")
            {
                r = Context.getProperty(PropertyName, "");
            }
            if (r == "" && PropertyName == "label")
            {
                r = Context.getProperty("name");
            }
            return r;

        }

        public string GetEditItem(string criteria)
        {
            //System.Diagnostics.Debugger.Break();
            string r = "";
            //criteria : itemtype:project&id:3852CE3CFAB447FFAB61343AB1951676&app:app2
            string[] param = criteria.Split('&');
            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':');
            string mode = GetParameterValue(criteria, "mode", '&', ':');
            string itemid = GetParameterValue(criteria, "itemid", '&', ':');
            string app = GetParameterValue(criteria, "app", '&', ':', "in_web_view");
            string source_config_id = GetParameterValue(criteria, "source_config_id", '&', ':', "");
            string Status = "false";
            string strPropertySet = "";
            Item item_PropertySet;
            string item_config_id = "";
            string aml = "";
            string r_templatename = "";

            if (mode == "add")
            {
                //itemid = inn.getNewID();
                //item_config_id = itemid;
            }


            //System.Diagnostics.Debugger.Break();;;
            GetPropertySet(itemtype, app, out strPropertySet, out item_PropertySet);


            string str_r = "";
            Item item_r;

            //header
            str_r += "<Header>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {
                Item tmp = item_PropertySet.getItemByIndex(i);
                str_r += "<p" + tmp.getProperty(app) + "_label";
                str_r += " is_required='" + tmp.getProperty("is_required", "0") + "'";
                str_r += ">";
                //str_r += tmp.getProperty("label","",GetUserInfo(UserInfo,"lang"));
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + tmp.getProperty(app) + "_label>";
            }
            str_r += "</Header>";


            if (mode == "add")
            {
                str_r += "<Item itemtype='" + itemtype + "' >";
                for (int j = 0; j < item_PropertySet.getItemCount(); j++)
                {
                    str_r += "\n";
                    Item item_Property = item_PropertySet.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    string AppPropertyLabel = GetMultilangProperty(item_Property, "label");
                    //str_r += FormatValue(item_Property, item_Property, AppPropertyName, "p" + item_Property.getProperty(app) + "_input");
                    str_r += FormatValue(item_Property, item_Property, AppPropertyName, "p" + item_Property.getProperty(app) + "_input", "#default_value");
                    str_r += "\n";
                }
                str_r += "</Item>\n";
            }
            else
            {

                item_r = GetCurrentItemById(itemtype, itemid);
                aml = "<AML>";
                aml += "<Item type='" + itemtype + "' id=\"" + item_r.getID() + "\" action='get' select='" + strPropertySet + ",permission_id,config_id' />";
                aml += "</AML>";

                item_r = inn.applyAML(aml);
                if (item_r.isError())
                {
                    //str_r = item_r.getErrorString();
                    //goto End;
                }

                Item tmp = item_r.getItemByIndex(0);
                itemid = item_r.getID();
                item_config_id = tmp.getProperty("config_id");



                str_r += "<Item itemtype='" + itemtype + "' itemid='" + tmp.getID() + "' >";
                for (int j = 0; j < item_PropertySet.getItemCount(); j++)
                {
                    //str_r += "<p" + tmp.getProperty("app2") + "_value>";
                    str_r += "\n";
                    Item item_Property = item_PropertySet.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    //string AppPropertyLabel = item_Property.getProperty("label","",GetUserInfo(UserInfo,"lang"));
                    string AppPropertyLabel = GetMultilangProperty(item_Property, "label");
                    str_r += FormatValue(tmp, item_Property, AppPropertyName, "p" + item_Property.getProperty(app) + "_input", "");
                    str_r += "\n";
                }
                str_r += "</Item>\n";
            }


            aml = "<AML>";
            aml += "<Item type='RelationshipType' action='get' orderBy='in_web_list' language='" + GetUserInfo(UserInfo, "lang") + "'>"; //relationship 一定是 app1 ,因为这个字段开在 Relationship type 上
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<in_web_list condition='gt'>0</in_web_list>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            if (!item_r.isError())
            {
                //取得Relationships
                str_r += "<RelationshipType>";
                for (int i = 0; i < item_r.getItemCount(); i++)
                {
                    Item tmp = item_r.getItemByIndex(i);
                    str_r += "<p" + tmp.getProperty("in_web_list") + ">";
                    //str_r += "<label>" + tmp.getProperty("label","",GetUserInfo(UserInfo,"lang")) + "</label>";
                    str_r += "<label>" + GetMultilangProperty(tmp, "label") + "</label>";
                    str_r += "<name>" + tmp.getProperty("name") + "</name>";

                    aml = "<AML>";
                    aml += "<Item type='Relationship View' action='get' select='start_page,form,parameters'>";
                    aml += "<source_id>" + tmp.getID() + "</source_id>";
                    aml += "<related_id>";
                    aml += "<Item type='identity' action='get' select='id'>";
                    aml += "<name>in_app</name>";
                    aml += "</Item>";
                    aml += "</related_id>";
                    aml += "</Item>";
                    aml += "</AML>";
                    string Salt = GetSalt();
                    Item tmp1 = inn.applyAML(aml);
                    if (!tmp1.isError())
                    {
                        tmp1 = tmp1.getItemByIndex(0);
                        string str_tmp = "";
                        str_tmp = tmp1.getProperty("start_page", "");
                        str_tmp = str_tmp.Replace("{#itemID}", "");
                        str_tmp = str_tmp.Replace("{#itemType}", itemtype);
                        str_tmp = str_tmp.Replace("{#salt}", Salt);
                        str_r += "<start_page><![CDATA[" + str_tmp + "]]></start_page>";

                        str_tmp = tmp1.getProperty("parameters", "");
                        str_tmp = str_tmp.Replace("{#itemID}", "");
                        str_tmp = str_tmp.Replace("{#itemType}", itemtype);
                        str_tmp = str_tmp.Replace("{#salt}", Salt);
                        str_r += "<parameters><![CDATA[" + str_tmp + "]]></parameters>";

                        str_tmp = tmp1.getProperty("form", "");
                        if (str_tmp != "")
                        {
                            str_tmp = inn.getItemById("Form", str_tmp).getProperty("name");
                        }
                        str_r += "<form>" + str_tmp + "</form>";
                    }
                    else
                    {
                        str_r += "<form>in_default_web_relationship</form>";
                    }



                    str_r += "</p" + tmp.getProperty("in_web_list") + ">";
                }
                str_r += "</RelationshipType>";
            }





            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' orderBy='display_priority'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>" + app + "</type>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            r_templatename = "in_default_web_view";
            if (item_r.getItemCount() > 0)
            {
                r_templatename = item_r.getItemByIndex(0).getRelatedItem().getProperty("name");
            }
            Status = "true";
            str_r += "<itemid>" + itemid + "</itemid>";
            str_r += "<itemtype>" + itemtype + "</itemtype>";



            //string lang = GetUserInfo(UserInfo, "lang");
            aml = "<AML>";
            aml += "<Item type='ItemType' action='get' language='" + lang + "' select='label'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            str_r += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";

            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            criteria += "<itemid>" + itemid + "</itemid>";
            criteria += "<item_config_id>" + item_config_id + "</item_config_id>";
            //criteria += "<source_config_id>" + source_config_id + "</source_config_id>";
            criteria += "<mode>" + mode + "</mode>";
            criteria += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);

            return r;
        }

        public string MergeItem(string criteria)
        {

            //System.Diagnostics.Debugger.Break();
            string updatecontent = GetParameterValue(criteria, "updatecontent", '&', ':');
            updatecontent = UnEscapeInnosoftChar(updatecontent);
            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':');
            string item_config_id = GetParameterValue(criteria, "item_config_id", '&', ':');
            string action = GetParameterValue(criteria, "action", '&', ':');
            string FilePropertyName = GetParameterValue(criteria, "FilePropertyName", '&', ':', "");
            string ServerFileIds = GetParameterValue(criteria, "ServerFileIds", '&', ':', "");

            string r = "";
            string Status = "false";
            string str_r = "";
            Item item_r = null;
            string r_templatename = "";

            string itemid = "";
            string mode = "";

            string aml = "";

            try
            {
                //item_r = GetCurrentItemByConfigId(itemtype, item_config_id);
                if (ServerFileIds != "")
                {
                    aml = "<AML>";
                    aml += "<Item type='" + itemtype + "' action='add'>";
                    aml += updatecontent;
                    aml += "</Item></AML>";
                    //代表有一个以上的File
                    string[] arrServerFileIds = ServerFileIds.Split(',');
                    for (int i = 0; i < arrServerFileIds.Length; i++)
                    {
                        string ServerFileId = arrServerFileIds[i];
                        if (ServerFileId != "")
                        {
                            string tmpAML = aml.Replace("#file_id", ServerFileId);
                            item_r = inn.applyAML(tmpAML);
                            str_r += item_r.getID() + ",";
                        }
                    }
                }
                else
                {
                    aml = "<AML>";
                    aml += "<Item type='" + itemtype + "' action='" + action + "' where=\"[" + itemtype.Replace(" ", "_") + "].[config_id] = '" + item_config_id + "' and [" + itemtype.Replace(" ", "_") + "].is_current ='1'\" >";
                    aml += updatecontent;
                    aml += "</Item></AML>";
                    item_r = inn.applyAML(aml);
                    str_r = item_r.getID();
                }
                str_r = str_r.TrimEnd(',');
                if (item_r.isError())
                    throw new Exception(item_r.getErrorString());
                Status = "true";
                str_r = "<itemid>" + str_r + "</itemid>";

                itemtype = item_r.getType();
                itemid = item_r.getID();
                item_config_id = item_r.getProperty("config_id");


                //template
                aml = "<AML>";
                aml += "<Item type='View' action='get' orderBy='display_priority'>";
                aml += "<source_id>";
                aml += "<Item type='ItemType' action='get' select='id'>";
                aml += "<name>" + item_r.getType() + "</name>";
                aml += "</Item>";
                aml += "</source_id>";
                aml += "<type>in_web_view</type>";
                aml += "</Item>";
                aml += "</AML>";

                Item item_template = inn.applyAML(aml);
                r_templatename = "in_default_web_view";
                if (item_template.getItemCount() > 0)
                {
                    r_templatename = item_template.getItemByIndex(0).getRelatedItem().getProperty("name");
                }



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {

            }
            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            criteria += "<itemid>" + itemid + "</itemid>";
            criteria += "<item_config_id>" + item_config_id + "</item_config_id>";
            criteria += "<mode>" + mode + "</mode>";
            criteria += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";

            r = BuildResponse(Status, str_r, r_templatename, criteria);
            return r;

        }


        public string GetItem(string criteria)
        {
            //System.Diagnostics.Debugger.Break();
            string r = "";
            //criteria : itemtype:project&id:3852CE3CFAB447FFAB61343AB1951676&app:app2
            string[] param = criteria.Split('&');
            string itemtype = param[0].Split(':')[1];
            string itemid = param[1].Split(':')[1];
            string app = GetParameterValue(criteria, "app", '&', ':', "in_web_view");
            string Status = "false";
            string strPropertySet = "";
            Item item_PropertySet;


            string aml = "";
            string r_templatename = "";

            string item_config_id = "";

            //System.Diagnostics.Debugger.Break();;;
            GetPropertySet(itemtype, app, out strPropertySet, out item_PropertySet);


            string str_r = "";


            Item item_r = GetCurrentItemById(itemtype, itemid);
            aml = "<AML>";
            aml += "<Item type='" + itemtype + "' id=\"" + item_r.getID() + "\" action='get' select='" + strPropertySet + ",permission_id,config_id' />";
            aml += "</AML>";

            item_r = inn.applyAML(aml);

            if (item_r.isError())
            {
                //str_r = item_r.getErrorString();
                //goto End;
            }
            itemid = item_r.getID();
            item_config_id = item_r.getProperty("config_id", "");


            //header
            str_r += "<Header>";
            for (int i = 0; i < item_PropertySet.getItemCount(); i++)
            {
                Item tmp = item_PropertySet.getItemByIndex(i);
                str_r += "<p" + tmp.getProperty(app) + "_label>";
                //str_r += tmp.getProperty("label","",GetUserInfo(UserInfo,"lang"));
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + tmp.getProperty(app) + "_label>";
            }
            str_r += "</Header>";



            for (int i = 0; i < item_r.getItemCount(); i++)
            {
                Item tmp = item_r.getItemByIndex(i);
                str_r += "<Item itemtype='" + itemtype + "' itemid='" + tmp.getID() + "' >";
                for (int j = 0; j < item_PropertySet.getItemCount(); j++)
                {
                    //str_r += "<p" + tmp.getProperty("app2") + "_value>";
                    str_r += "\n";
                    Item item_Property = item_PropertySet.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    //string AppPropertyLabel = item_Property.getProperty("label","",GetUserInfo(UserInfo,"lang"));
                    string AppPropertyLabel = GetMultilangProperty(item_Property, "label");
                    str_r += FormatValue(tmp, item_Property, AppPropertyName, "p" + item_Property.getProperty(app) + "_value", "");
                    str_r += "\n";


                }
                str_r += "</Item>\n";
            }

            aml = "<AML>";
            aml += "<Item type='RelationshipType' action='get' orderBy='in_web_list' language='" + GetUserInfo(UserInfo, "lang") + "'>"; //relationship 一定是 app1 ,因为这个字段开在 Relationship type 上
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<in_web_list condition='gt'>0</in_web_list>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            if (!item_r.isError())
            {
                //取得Relationships
                str_r += "<RelationshipType>";
                for (int i = 0; i < item_r.getItemCount(); i++)
                {
                    Item tmp = item_r.getItemByIndex(i);
                    str_r += "<p" + tmp.getProperty("in_web_list") + ">";
                    //str_r += "<label>" + tmp.getProperty("label","",GetUserInfo(UserInfo,"lang")) + "</label>";
                    str_r += "<label>" + GetMultilangProperty(tmp, "label") + "</label>";
                    str_r += "<name>" + tmp.getProperty("name") + "</name>";

                    aml = "<AML>";
                    aml += "<Item type='Relationship View' action='get' select='start_page,form,parameters'>";
                    aml += "<source_id>" + tmp.getID() + "</source_id>";
                    aml += "<related_id>";
                    aml += "<Item type='identity' action='get' select='id'>";
                    aml += "<name>in_app</name>";
                    aml += "</Item>";
                    aml += "</related_id>";
                    aml += "</Item>";
                    aml += "</AML>";
                    string Salt = GetSalt();
                    Item tmp1 = inn.applyAML(aml);
                    if (!tmp1.isError())
                    {
                        tmp1 = tmp1.getItemByIndex(0);
                        string str_tmp = "";
                        str_tmp = tmp1.getProperty("start_page", "");
                        str_tmp = str_tmp.Replace("{#itemID}", itemid); //将 start_page 内的变数至换成正确的值
                        str_tmp = str_tmp.Replace("{#itemType}", itemtype);
                        str_tmp = str_tmp.Replace("{#salt}", Salt);
                        str_r += "<start_page><![CDATA[" + str_tmp + "]]></start_page>";

                        str_tmp = tmp1.getProperty("parameters", "");
                        str_tmp = str_tmp.Replace("{#itemID}", itemid);
                        str_tmp = str_tmp.Replace("{#itemType}", itemtype);
                        str_tmp = str_tmp.Replace("{#salt}", Salt);
                        str_r += "<parameters><![CDATA[" + str_tmp + "]]></parameters>";

                        str_tmp = tmp1.getProperty("form", "");
                        if (str_tmp != "")
                        {
                            str_tmp = inn.getItemById("Form", str_tmp).getProperty("name");
                        }
                        str_r += "<parameter_form>" + str_tmp + "</parameter_form>";
                    }
                    else
                    {
                        str_r += "<form>in_default_web_relationship</form>";
                    }


                    str_r += "</p" + tmp.getProperty("in_web_list") + ">";
                }
                str_r += "</RelationshipType>";
            }

            //GetActionButton
            aml = "<AML>";
            aml += "<Item type='Item Action' action='get' select='in_web_list,related_id(type,label,body)' orderBy='in_web_list'>";
            aml += "<source_id>" + inn.getItemByKeyedName("ItemType", itemtype).getID() + "</source_id>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            string in_web_list = "";
            if (!item_r.isError() && item_r.getItemCount() > 0)
            {
                str_r += "<Actions>";

                for (int k = 0; k < item_r.getItemCount(); k++)
                {
                    Item tmp_action = item_r.getItemByIndex(k).getRelatedItem();
                    in_web_list = item_r.getItemByIndex(k).getProperty("in_web_list");

                    if (tmp_action == null)
                        continue;
                    if (tmp_action.getProperty("type") == "app_item")
                    {
                        string body = tmp_action.getProperty("body");
                        string url = getParameter(body, "url", ';');
                        //string name = tmp_action.getProperty("label","",GetUserInfo(UserInfo,"lang"));
                        string name = GetMultilangProperty(tmp_action, "label");

                        url = url.Replace("{@id}", itemid);
                        url = url.Replace("{@itemtype}", itemtype);

                        str_r += "<p" + in_web_list + ">";
                        str_r += "<url><![CDATA[" + url + "]]></url>";
                        str_r += "<label>" + name + "</label>";
                        str_r += "</p" + in_web_list + ">";
                    }
                }
                str_r += "</Actions>";
            }

            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' orderBy='display_priority'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>in_web_view</type>";
            aml += "</Item>";
            aml += "</AML>";

            Item item_template = inn.applyAML(aml);
            r_templatename = "in_default_web_view";
            if (item_template.getItemCount() > 0)
            {
                r_templatename = item_template.getItemByIndex(0).getRelatedItem().getProperty("name");
            }
            Status = "true";
            str_r += "<itemid>" + itemid + "</itemid>";
            str_r += "<itemtype>" + itemtype + "</itemtype>";
            //str_r += "<itemtypeName>" + "项目任务" + "</itemtypeName>"; //1217批注掉


            //string lang = GetUserInfo(UserInfo, "lang");
            aml = "<AML>";
            aml += "<Item type='ItemType' action='get' language='" + lang + "' select='label'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            str_r += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";

            //验证目前的USER是否有权限编辑本对象
            bool canedit = CanEditThisItem(itemtype, itemid);

            //验证是否可以启动流程
            //Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
            Item Itmcansendflow = _InnH.AllowSendFlow(itemtype, itemid, true);
            bool cansendflow = false;
            if (!Itmcansendflow.isError())
                cansendflow = true;

            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            criteria += "<itemid>" + itemid + "</itemid>";
            criteria += "<item_config_id>" + item_config_id + "</item_config_id>";
            criteria += "<itemtypelabel>" + GetMultilangProperty(item_r, "label") + "</itemtypelabel>";
            criteria += "<canedit>" + canedit + "</canedit>";
            criteria += "<cansendflow>" + cansendflow + "</cansendflow>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);

            return r;
        }

        private bool CanEditThisItem(string ItemType, string ItemId)
        {
            Item itmItem = inn.getItemById(ItemType, ItemId);
            bool IsAllowEdit = false;
            try
            {
                itmItem.lockItem();
                IsAllowEdit = true;
                itmItem.unlockItem();
            }
            catch (Exception ex)
            {
                IsAllowEdit = false;
            }
            return IsAllowEdit;

        }

        public string getParameter(string parambody, string paramname, char seperater)
        {
            string r = "";
            string[] paramarr = parambody.Split(seperater);
            for (int i = 0; i < paramarr.Length; i++)
            {
                string tmp = paramarr[i];
                string tmp_name = tmp.Split(':')[0].Trim();
                if (tmp_name == paramname)
                    return tmp.Split(':')[1].Trim();
            }
            return r;
        }




        public string GetTemplates(string criteria)
        {
            //System.Diagnostics.Debugger.Break();;;

            string r = "";
            //criteria : clienttemplates:in_Item_App2,1&in_web_list,1&in_web_view,2
            string[] clienttemplates = criteria.Replace("clienttemplates:", "").Split('&');
            string r_templatenames = "";
            string aml = "<AML>";
            aml += "<Item type='Form' action='get' where='[Form].[in_useweb_ver]>0' select='name,in_useweb_ver'>";
            aml += "<Relationships>";
            aml += "<Item type='Body' action='get' select='id'>";
            aml += "<Relationships>";
            aml += "<Item type='Field' action='get' select='name,html_code' where=\"[Field].[name] in ('in_template','in_parameter','in_template_header')\" />";
            aml += "</Relationships>";
            aml += "</Item>";
            aml += "</Relationships>";

            aml += "</Item>";
            aml += "</AML>";

            Item r_item = inn.applyAML(aml);
            string str_r = "";
            if (!r_item.isError())
            {
                for (int k = 0; k < r_item.getItemCount(); k++)
                {
                    string tmp_name = r_item.getItemByIndex(k).getProperty("name") + "," + r_item.getItemByIndex(k).getProperty("in_useweb_ver");

                    r_templatenames += tmp_name + "&";
                    if (criteria.IndexOf(tmp_name) > -1)
                    {
                        continue;
                    }

                    str_r += GetTemplate(r_item.getItemByIndex(k).getProperty("name"));
                }
            }
            r_templatenames = r_templatenames.TrimEnd('&');

            r = "<response>";
            r += "<status>true</status>";
            r += "<result>";
            r += "<templatenames><![CDATA[" + r_templatenames + "]]></templatenames>";
            r += str_r;
            r += "</result>";
            r += "</response>";

            return r;
        }

        public string GetTemplate(string TemplateName)
        {
            //System.Diagnostics.Debugger.Break();;;
            string str_r = "";
            string aml = "<AML>";
            aml += "<Item type='Form' action='get' where=\"[Form].[name]='" + TemplateName + "'\">";
            aml += "<Relationships>";
            aml += "<Item type='Body' action='get' select='id'>";
            aml += "<Relationships>";
            aml += "<Item type='Field' action='get' select='name,html_code' where=\"[Field].[name] in ('in_template','in_parameter','in_template_header','in_template_criteria','in_template_edit')\" />";
            aml += "</Relationships>";
            aml += "</Item>";
            aml += "</Relationships>";
            aml += "</Item>";
            aml += "</AML>";

            Item r_item = inn.applyAML(aml);

            if (!r_item.isError())
            {
                string tmp_name = r_item.getProperty("name") + "," + r_item.getProperty("in_useweb_ver");
                Item tmp_Fields = r_item.getRelationships("Body").getItemByIndex(0).getRelationships("Field");
                str_r += "<template>";
                str_r += "<name>" + r_item.getProperty("name") + "</name>";
                for (int m = 0; m < tmp_Fields.getItemCount(); m++)
                {
                    Item tmp_Field = tmp_Fields.getItemByIndex(m);
                    //string escapxml = System.Security.SecurityElement.Escape(tmp_Field.getProperty("html_code"));
                    if (tmp_Field.getProperty("name") == "in_template")
                    {
                        str_r += "<in_template><![CDATA[" + tmp_Field.getProperty("html_code") + "]]></in_template>";
                    }
                    if (tmp_Field.getProperty("name") == "in_template_header")
                    {
                        str_r += "<in_template_header><![CDATA[" + tmp_Field.getProperty("html_code") + "]]></in_template_header>";
                    }
                    if (tmp_Field.getProperty("name") == "in_template_criteria")
                    {
                        str_r += "<in_template_criteria><![CDATA[" + tmp_Field.getProperty("html_code") + "]]></in_template_criteria>";
                    }
                    if (tmp_Field.getProperty("name") == "in_template_edit")
                    {
                        str_r += "<in_template_edit><![CDATA[" + tmp_Field.getProperty("html_code") + "]]></in_template_edit>";
                    }
                    if (tmp_Field.getProperty("name") == "in_parameter")
                    {
                        str_r += "<in_parameter><![CDATA[" + tmp_Field.getProperty("html_code") + "]]></in_parameter>";
                    }
                }
                str_r += "</template>";
            }


            return str_r;
        }

        public string GetRel(string criteria)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            //criteria : itemtype:project team&where:[Project_Team].[source_id]='3852CE3CFAB447FFAB61343AB1951676'&app:app2
            string[] param = criteria.Split('&');

            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':', ""); //string itemtype = param[0].Split(':')[1];
            string where = GetParameterValue(criteria, "where", '&', ':', ""); //string str_where = param[1].Split(':')[1];
            string app = GetParameterValue(criteria, "app", '&', ':', "in_web_relationship"); //app2:Itemtype property, app3:Relationship property

            string source_itemtype = GetParameterValue(criteria, "source_itemtype", '&', ':', "");
            string source_config_id = GetParameterValue(criteria, "source_config_id", '&', ':', "");

            string r_templatename = "";
            string str_r = "";
            string Status = "false";

            if(IsReverseRelationship(itemtype))
            {
                r = GetReverseRel(criteria);
                return r;
            }

            string strPropertySet_rel = "";
            Item item_PropertySet_rel;
            GetPropertySet(itemtype, "in_web_relationship", out strPropertySet_rel, out item_PropertySet_rel);

            string aml = "";
            aml = "<AML>";
            aml += "<Item type='RelationshipType' action='get' select='related_id(name)'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item></AML>";

            string itemtype_relitem = "";
            string strPropertySet_relitem = "";

            Item item_PropertySet_relitem = null;
            Item item_r = inn.applyAML(aml);
            if (!item_r.isError() && item_r.getRelatedItem() != null)
            {
                itemtype_relitem = item_r.getRelatedItem().getProperty("name");
                strPropertySet_relitem = "";
                GetPropertySet(itemtype_relitem, "in_web_relationship", out strPropertySet_relitem, out item_PropertySet_relitem);
            }

            if (item_r.isError())
            {
                //str_r = item_r.getErrorString();
                //goto End;
            }



            aml = "<AML>";
            aml += "<Item type='" + itemtype + "' action='get' where=\"" + where + "\" select ='in_company,source_id(config_id)," + strPropertySet_rel;
            if (strPropertySet_relitem != "")
            {
                aml += ",related_id(" + strPropertySet_relitem + ")";
            }
            aml += "' />";
            //aml += "<source_id>" + GetCurrentItemById(source_itemtype, source_config_id).getID() + "</source_id>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);




            if (item_r.isError())
            {
                //str_r = item_r.getErrorString();
                //goto End;
            }
            //strSoureConfigId = item_r.getPropertyItem("source_id").getProperty("config_id");
            //header
            str_r += "<Header>";

            //Relationship 字段的排列方式：先RelatedItem的字段 ,后面再接Relationship 的字段

            int relitemPropertyIndex = 0;
            int relPropertyIndex = 0;

            if (strPropertySet_relitem != "")
            {
                for (int i = 0; i < item_PropertySet_relitem.getItemCount(); i++)
                {
                    Item tmp = item_PropertySet_relitem.getItemByIndex(i);
                    relitemPropertyIndex = int.Parse(tmp.getProperty("in_web_relationship", "0"));
                    str_r += "<p" + relitemPropertyIndex.ToString() + "_label>";
                    //str_r += tmp.getProperty("label", "", lang);
                    str_r += GetMultilangProperty(tmp, "label");
                    str_r += "</p" + relitemPropertyIndex.ToString() + "_label>";
                }
                str_r += "<related_id_label>";
                str_r += Getl10nValue("in_ui_resources", "bcsapp.cs.bcsapp3_RelatedItemLabel", "关联物件");
                str_r += "</related_id_label>";
            }

            for (int i = 0; i < item_PropertySet_rel.getItemCount(); i++)
            {
                Item tmp = item_PropertySet_rel.getItemByIndex(i);
                relPropertyIndex = relitemPropertyIndex + int.Parse(tmp.getProperty("in_web_relationship", "0"));
                str_r += "<p" + relPropertyIndex.ToString() + "_label>";
                //str_r += tmp.getProperty("label", "", lang);
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + relPropertyIndex.ToString() + "_label>";
            }
            str_r += "<id_label>";
            str_r += Getl10nValue("in_ui_resources", "bcsapp.cs.bcsapp3_RelationshipLabel", "关系类型");
            str_r += "</id_label>";


            str_r += "</Header>";

            //2017-08-24 : in_company機制
            //in_company機制
            /*公司別過濾效果:
	            1.一般使用者搜尋的時候,一律補上公司別=自己 + 公司別為空值
	            2.若屬於 AllCompany 群組,則不需過慮公司別
	            3.若找不到 AllCompany,則不需要公司別過濾效果
            */

            var IsAllCompany = "";
            var strMyCompany = "";

            if (IsAllCompany == "")
            {
                var d_aml = "<AML>";
                d_aml += "<Item type='Identity' action='get'>";
                d_aml += "<name>AllCompany</name>";
                d_aml += "</Item></AML>";

                Item itmAllCompany = inn.applyAML(d_aml);
                if (!itmAllCompany.isError())
                {
                    var strAllCompanyId = itmAllCompany.getID();
                    if (inn.applyMethod("In_GetIdentityList", "<user_id>" + inn.getConnection().getUserID() + "</user_id>").getResult().Contains(strAllCompanyId))
                    {
                        IsAllCompany = "true";
                    }
                    else
                    {
                        IsAllCompany = "false";
                        if (strMyCompany == "")
                        {
                            d_aml = "<AML>";
                            d_aml += "<Item type='User' action='get'>";
                            d_aml += "<id>" + inn.getConnection().getUserID() + "</id>";
                            d_aml += "</Item></AML>";
                            Item itmUser = inn.applyAML(d_aml);

                            strMyCompany = itmUser.getProperty("in_company", "");
                        }
                    }

                }
                else
                {
                    IsAllCompany = "true";
                }

            }


            Item item_Property;
            for (int i = 0; i < item_r.getItemCount(); i++)
            {
                Item tmp = item_r.getItemByIndex(i);
                //如果不是本公司的就直接跳掉
                if(IsAllCompany=="false")
                {
                    if(tmp.getProperty("related_id","")!="") //關聯物件必須是本公司
                    {
                        string strRelatedCompany = tmp.getPropertyItem("related_id").getProperty("in_company", "");

                        if (strRelatedCompany != "" && strRelatedCompany != strMyCompany)
                            continue;
                    }                    

                    if(tmp.getProperty("in_company","")!="") //關聯必須是本公司
                    {
                        if (tmp.getProperty("in_company", "") != strMyCompany)
                            continue;
                    }

                }
                str_r += "<Item itemtype='" + itemtype + "' itemid='" + tmp.getID() + "' source_itemtype='" + source_itemtype + "' source_config_id='" + source_config_id + "' >";
                str_r += FormatValue(tmp, "id", "item", tmp.getType(), "id_value", "");

                //先处理 Related_Item字段
                if (strPropertySet_relitem != "")
                {
                    tmp = item_r.getItemByIndex(i).getRelatedItem();
                    string strNodeName = "";
                    for (int j = 0; j < item_PropertySet_relitem.getItemCount(); j++)
                    {
                        str_r += "\n";
                        item_Property = item_PropertySet_relitem.getItemByIndex(j);
                        string AppPropertyName = item_Property.getProperty("name");
                        string appvalue = item_Property.getProperty("in_web_relationship");
                        relitemPropertyIndex = int.Parse(item_Property.getProperty("in_web_relationship", "0"));
                        
                        if (tmp == null)
                        {
                            //代表related item 是 restricted
                            //str_r += "<p" + relitemPropertyIndex.ToString() + "_value>" + Getl10nValue("ui_resource", "bcsapp.cs.Restricted", "限制存取") + "</p" + relitemPropertyIndex.ToString() + "_value>";
                             strNodeName = "p" + relitemPropertyIndex.ToString() + "_value";
                            if (AppVer == "2")
                                strNodeName = "c_" + AppPropertyName;
                            str_r += "<" + strNodeName + ">" + "" + "</" + strNodeName + ">";
                        }
                        else
                        {
                             strNodeName = "p" + relPropertyIndex.ToString() + "_value";
                            if (AppVer == "2")
                                strNodeName = "c_" + AppPropertyName;
                            str_r += FormatValue(tmp, item_Property, AppPropertyName, strNodeName, "");
                        }
                        str_r += "\n";
                    }
                    str_r += "\n";
                    string relitem_type = (tmp == null) ? "" : tmp.getType();
                     strNodeName = "related_id_value";
                    if (AppVer == "2")
                        strNodeName = "related_id";
                    str_r += FormatValue(tmp, "id", "item", relitem_type, strNodeName, "");

                }
                tmp = item_r.getItemByIndex(i);
                for (int j = 0; j < item_PropertySet_rel.getItemCount(); j++)
                {
                    str_r += "\n";
                    item_Property = item_PropertySet_rel.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    string appvalue = item_Property.getProperty("in_web_relationship");
                    relPropertyIndex = relitemPropertyIndex + int.Parse(item_Property.getProperty("in_web_relationship", "0"));
                    string strNodeName = "p" + relPropertyIndex.ToString() + "_value";
                    if (AppVer == "2")
                        strNodeName = AppPropertyName;
                    str_r += FormatValue(tmp, item_Property, AppPropertyName, strNodeName, "");
                    str_r += "\n";
                }
                str_r += "\n";

                str_r += "</Item>\n";
            }

            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' select='name'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>in_web_relationship</type>";
            aml += "</Item>";
            aml += "</AML>";

            item_r = inn.applyAML(aml);
            r_templatename = "in_default_web_relationship";
            if (item_r.getItemCount() > 0)
            {
                r_templatename = item_r.getItemByIndex(0).getRelatedItem().getProperty("name");

            }


            Status = "true";

            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<source_itemtype>" + source_itemtype + "</source_itemtype>";
            criteria += "<source_config_id>" + source_config_id + "</source_config_id>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);

            return r;
        }

        public bool IsReverseRelationship(string RelationshipTypeName)
        {
            string aml = "";
            aml = "<AML>";
            aml += "<Item type='Relationship View' action='get'>";
            aml += "<source_id>";
            aml += "<Item type='RelationshipType' action='get' select='id'>";
            aml += "<name>" + RelationshipTypeName + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<start_page>../Solutions/PLM/scripts/ReverseItemsGrid.html</start_page>";
            aml += "</Item></AML>";

            Item itmRel = inn.applyAML(aml);
            if (itmRel.isError())
                return false;
            else
                return true;
        }

        public string GetReverseRel(string criteria)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            //criteria : itemtype:project team&where:[Project_Team].[source_id]='3852CE3CFAB447FFAB61343AB1951676'&app:app2
            string[] param = criteria.Split('&');

            string itemtype = GetParameterValue(criteria, "itemtype", '&', ':', ""); //string itemtype = param[0].Split(':')[1];
            string where = GetParameterValue(criteria, "where", '&', ':', ""); //string str_where = param[1].Split(':')[1];
            string app = GetParameterValue(criteria, "app", '&', ':', "in_web_relationship"); //app2:Itemtype property, app3:Relationship property

            string source_itemtype = GetParameterValue(criteria, "source_itemtype", '&', ':', "");
            string source_config_id = GetParameterValue(criteria, "source_config_id", '&', ':', "");

            Item ItmSourceItem = GetCurrentItemByConfigId(source_itemtype, source_config_id);


            string r_templatename = "";
            string str_r = "";
            string Status = "false";


            string strPropertySet_rel = "";
            Item item_PropertySet_rel;

          
            string aml = "";
            aml = "<AML>";
            aml += "<Item type='Relationship View' action='get'>";
            aml += "<source_id>";
            aml += "<Item type='RelationshipType' action='get' select='id'>";
            aml += "<name>" + itemtype + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<start_page>../Solutions/PLM/scripts/ReverseItemsGrid.html</start_page>";
            aml += "</Item></AML>";

            Item itmRelView = inn.applyAML(aml);
            string strParam = itmRelView.getItemByIndex(0).getProperty("parameters", "");
            strParam = strParam.Trim('\'');
            //'ITName='+itemTypeName+'&itemID='+itemID+'&reverseITName=In_TimeRecord&reversePropertyName=in_workorder'
            string reverseITName = GetParameterValue(strParam, "reverseITName", '&', '=');
            string reversePropertyName = GetParameterValue(strParam, "reversePropertyName", '&', '=',"");

            if(reversePropertyName!="")
            {
                aml = "<AML>";
                aml += "	<Item type='" + reverseITName + "' action='get'>";
                aml += "		<" + reversePropertyName + " condition='eq'>" + ItmSourceItem.getID() + "</" + reversePropertyName + ">";
                aml += " </Item>";
                aml += "</AML>";
            }





            string itemtype_relitem = "";
            string strPropertySet_relitem = "";

            Item item_PropertySet_relitem = null;

            Item itmReversedItems = inn.applyAML(aml);

            GetPropertySet(reverseITName, "in_web_relationship", out strPropertySet_rel, out item_PropertySet_rel);
            if (itmReversedItems.isError())
            {
                //str_r = item_r.getErrorString();
                //goto End;
            }
            //strSoureConfigId = item_r.getPropertyItem("source_id").getProperty("config_id");
            //header
            str_r += "<Header>";

            //Relationship 字段的排列方式：先RelatedItem的字段 ,后面再接Relationship 的字段

            int relitemPropertyIndex = 0;
            int relPropertyIndex = 0;

            if (strPropertySet_relitem != "")
            {
                for (int i = 0; i < item_PropertySet_relitem.getItemCount(); i++)
                {
                    Item tmp = item_PropertySet_relitem.getItemByIndex(i);
                    relitemPropertyIndex = int.Parse(tmp.getProperty("in_web_relationship", "0"));
                    str_r += "<p" + relitemPropertyIndex.ToString() + "_label>";
                    //str_r += tmp.getProperty("label", "", lang);
                    str_r += GetMultilangProperty(tmp, "label");
                    str_r += "</p" + relitemPropertyIndex.ToString() + "_label>";
                }
                str_r += "<related_id_label>";
                str_r += Getl10nValue("in_ui_resources", "bcsapp.cs.bcsapp3_RelatedItemLabel", "关联物件");
                str_r += "</related_id_label>";
            }

            for (int i = 0; i < item_PropertySet_rel.getItemCount(); i++)
            {
                Item tmp = item_PropertySet_rel.getItemByIndex(i);
                relPropertyIndex = relitemPropertyIndex + int.Parse(tmp.getProperty("in_web_relationship", "0"));
                str_r += "<p" + relPropertyIndex.ToString() + "_label>";
                //str_r += tmp.getProperty("label", "", lang);
                str_r += GetMultilangProperty(tmp, "label");
                str_r += "</p" + relPropertyIndex.ToString() + "_label>";
            }
            str_r += "<id_label>";
            str_r += Getl10nValue("in_ui_resources", "bcsapp.cs.bcsapp3_RelationshipLabel", "关系类型");
            str_r += "</id_label>";


            str_r += "</Header>";


            Item item_Property;
            for (int i = 0; i < itmReversedItems.getItemCount(); i++)
            {
                Item itmReversedItem = itmReversedItems.getItemByIndex(i);
                str_r += "<Item itemtype='" + itmReversedItem.getType() + "' itemid='" + itmReversedItem.getID() + "' source_itemtype='" + source_itemtype + "' source_config_id='" + source_config_id + "' >";
                str_r += FormatValue(itmReversedItem, "id", "item", itmReversedItem.getType(), "id_value", "");
                for (int j = 0; j < item_PropertySet_rel.getItemCount(); j++)
                {
                    str_r += "\n";
                    item_Property = item_PropertySet_rel.getItemByIndex(j);
                    string AppPropertyName = item_Property.getProperty("name");
                    string appvalue = item_Property.getProperty("in_web_view");
                    relPropertyIndex = relitemPropertyIndex + int.Parse(item_Property.getProperty("in_web_view", "0"));
                    string strNodeName = "p" + relPropertyIndex.ToString() + "_value";
                    if (AppVer == "2")
                        strNodeName = AppPropertyName;
                    str_r += FormatValue(itmReversedItem, item_Property, AppPropertyName, strNodeName, "");
                    str_r += "\n";
                }
                str_r += "\n";

                str_r += "</Item>\n";
            }

            //template
            aml = "<AML>";
            aml += "<Item type='View' action='get' select='name'>";
            aml += "<source_id>";
            aml += "<Item type='ItemType' action='get' select='id'>";
            aml += "<name>" + reverseITName + "</name>";
            aml += "</Item>";
            aml += "</source_id>";
            aml += "<type>in_web_relationship</type>";
            aml += "</Item>";
            aml += "</AML>";

            Item itmTemplate = inn.applyAML(aml);
            r_templatename = "in_default_web_relationship";
            if (itmTemplate.getItemCount() > 0)
            {
                r_templatename = itmTemplate.getItemByIndex(0).getRelatedItem().getProperty("name");

            }


            Status = "true";

            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            criteria += "<source_itemtype>" + source_itemtype + "</source_itemtype>";
            criteria += "<source_config_id>" + source_config_id + "</source_config_id>";
            criteria += "<itemtype>" + itemtype + "</itemtype>";
            criteria += "<is_reversed_relationship>1</is_reversed_relationship>";
            r = BuildResponse(Status, str_r, r_templatename, criteria);

            return r;
        }

        public string FormatValue(Item ContextItem, Item PropertyItem, string PropertyName, string TagPrefix, string OverwriteValue)
        {
            //System.Diagnostics.Debugger.Break();;;
            string datatype = PropertyItem.getProperty("data_type");
            string str_r = "";
            string param = "";

            switch (datatype)
            {
                case "item":
                    param = PropertyItem.getPropertyAttribute("data_source", "keyed_name","");
                    param = param.Split(' ')[0]; //因為有一些keyedname會是: 'source_id xxxxxxxxxxx'
                    str_r = FormatValue(ContextItem, PropertyName, datatype, param, TagPrefix, OverwriteValue);
                    break;
                case "date":
                    param = PropertyItem.getProperty("pattern");
                    str_r = FormatValue(ContextItem, PropertyName, datatype, param, TagPrefix, OverwriteValue);
                    break;
                case "filter list":
                case "list":
                    param = PropertyItem.getPropertyAttribute("data_source", "keyed_name");
                    param = param.Split(' ')[0]; //因為有一些keyedname會是: 'source_id xxxxxxxxxxx'
                    str_r = FormatValue(ContextItem, PropertyName, datatype, param, TagPrefix, OverwriteValue);
                    break;
                case "foreign":
                    string strDataSourceProperty = PropertyItem.getPropertyAttribute("data_source", "keyed_name");
                    strDataSourceProperty = strDataSourceProperty.Split(' ')[0]; //因為有一些keyedname會是: 'source_id xxxxxxxxxxx'
                    string strSourceItemType = ContextItem.getPropertyAttribute(strDataSourceProperty, "type");
                    string strPropertyValue = ContextItem.getProperty(strDataSourceProperty, "");
                    if (strPropertyValue != "")
                    {
                        Item itmSourceItem = inn.getItemById(strSourceItemType, strPropertyValue);
                        Item itmSourceProperty = inn.getItemById("Property", PropertyItem.getProperty("foreign_property"));
                        str_r = FormatValue(itmSourceItem, itmSourceProperty, itmSourceProperty.getProperty("name"), TagPrefix, OverwriteValue);
                        str_r = str_r.Replace("otherattributes=''", "is_foreign='true'");
                    }
                    else
                        str_r = FormatValue(ContextItem, PropertyName, datatype, "", TagPrefix, OverwriteValue);
                    break;
                default:
                    str_r = FormatValue(ContextItem, PropertyName, datatype, "", TagPrefix, OverwriteValue);
                    break;
            }
            return str_r;

        }



        public string FormatValue(Item ContextItem, string PropertyName, string datatype, string PropertyParam, string TagPrefix, string OverwriteValue)
        {
            //System.Diagnostics.Debugger.Break();;;

            //PropertyParam:
            //datatype是item, PropertyParam代表 data_source,(e.g. Identity, User, File)
            //datatype是date, PropertyParam代表 partten,(e.g. short_date,short_date_time...)

            //datatype是reldetail, 这是内建的功能,包含了relationship的信息与relatedid的信息

            string str_r = "";
            str_r = "<" + TagPrefix + " datatype='" + datatype + "' propertyname='" + PropertyName + "' ";
            string tmpPropertyValue = "";
            string oriPropertyValue = "";
            string r_PropertyValue = "";
            if (ContextItem == null)
            {
                tmpPropertyValue = "";
                goto GoEnd;
            }

            bool IsInputMode = false;
            if (TagPrefix.IndexOf("_input") == TagPrefix.Length - 6)
            {
                IsInputMode = true;
            }


            //string datatype = PropertyItem.getProperty("data_type");
            if (PropertyName == "label" || PropertyName == "default_value")
            {
                oriPropertyValue = GetMultilangProperty(ContextItem, PropertyName);
            }
            else
            {
                oriPropertyValue = ContextItem.getProperty(PropertyName, "");
            }

            switch (OverwriteValue)
            {
                case "#default_value":
                    oriPropertyValue = ContextItem.getProperty("default_value", "");
                    break;
                case "#empty":
                    oriPropertyValue = "";
                    break;
                case "":
                    break;
                default:
                    oriPropertyValue = OverwriteValue;
                    break;

            }



            tmpPropertyValue = oriPropertyValue;
            //r_PropertyValue = "<![CDATA[" + oriPropertyValue + "]]>";
            r_PropertyValue = System.Web.HttpUtility.HtmlEncode(oriPropertyValue);
            //string str_r = "<p" + appindex + "_value datatype='" + datatype + "'";  
            string aml = "";
            Item r_item;
            switch (datatype)
            {
                case "item":
                    //tmpPropertyValue = ContextItem.getPropertyAttribute(PropertyName, "keyed_name", "");

                    string data_source = PropertyParam;
                    string PropertyType = ContextItem.getPropertyAttribute(PropertyName, "type", "");


                    if (IsPloyItem(PropertyType) && oriPropertyValue != "")
                    {

                        //如果该Prroperty 的  Itemtype 是 poly,例如 change controlled items,则要找到真正的Itemtype name
                        aml = "<AML>";
                        aml += "<Item type='" + PropertyType + "' action='get' select='itemtype'>";
                        aml += "<id>" + oriPropertyValue + "</id>";
                        aml += "</Item>";
                        aml += "</AML>";
                        r_item = inn.applyAML(aml); //取得 该 PolyItem 的 itemtype id 

                        aml = "<AML>";
                        aml += "<Item type='ItemType' action='get' select='name'>";
                        aml += "<id>" + r_item.getProperty("itemtype", "") + "</id>";
                        aml += "</Item>";
                        aml += "</AML>";
                        r_item = inn.applyAML(aml); //取得 该 PolyItem 的 itemtype id 对应的  Itemtype name (例如 Document)

                        PropertyParam = r_item.getProperty("name");
                    }

                    str_r += " itemtype='" + PropertyParam + "' itemid='" + ContextItem.getProperty(PropertyName) + "'";
                    str_r += " label='" + System.Security.SecurityElement.Escape(ContextItem.getPropertyAttribute(PropertyName, "keyed_name", "")) + "'";

                    if (data_source == "Identity") //E582AB17663F4EF28460015B2BE9E094:identity
                    {
                        //System.Diagnostics.Debugger.Break();;;
                        aml = "<AML>";
                        aml += "<Item type='alias' action='get' select='source_id(email,cell),related_id(keyed_name)'>";
                        aml += "<related_id>" + ContextItem.getProperty(PropertyName, "") + "</related_id>";
                        aml += "</Item>";
                        aml += "</AML>";

                        r_item = inn.applyAML(aml);

                        if (!r_item.isError())
                        {
                            string tel = r_item.getPropertyItem("source_id").getProperty("cell");
                            string email = r_item.getPropertyItem("source_id").getProperty("email");

                            str_r += " tel='" + tel + "' email='" + email + "'";
                        }
                    }

                    if (data_source == "User")
                    {
                        aml = "<AML>";
                        aml += "<Item type='User' action='get' select='email,cell,keyed_name'>";
                        aml += "<id>" + ContextItem.getProperty(PropertyName, "") + "</id>";
                        aml += "</Item>";
                        aml += "</AML>";

                        r_item = inn.applyAML(aml);

                        if (!r_item.isError())
                        {
                            string tel = r_item.getProperty("cell", "");
                            string email = r_item.getProperty("email", "");

                            str_r += " tel='" + tel + "' email='" + email + "'";
                        }
                    }



                    if (data_source == "File") //E582AB17663F4EF28460015B2BE9E094:identity
                    {
                        aml = "<AML>";
                        aml += "<Item type='File' action='get' id='" + ContextItem.getProperty(PropertyName, "no") + "'>";
                        aml += "</Item>";
                        aml += "</AML>";

                        if (ContextItem.getProperty(PropertyName, "no") == "no")
                            break;

                        r_item = inn.applyAML(aml);
                        if (!r_item.isError())
                        {
                            double file_size = double.Parse(r_item.getProperty("file_size", "0"));
                            file_size = file_size / 1024;
                            str_r += " file_size='" + file_size.ToString("###,###.####") + "'";
                        }
                    }

                    break;
                case "date":
                    string pattern = PropertyParam;

                    if (pattern == "short_date")
                    {
                        tmpPropertyValue = tmpPropertyValue.Split('T')[0];
                    }
                    if (pattern == "short_date_time")
                    {
                        tmpPropertyValue = tmpPropertyValue.Replace("T", " ");
                    }
                    if (pattern == "long_date")
                    {
                        tmpPropertyValue = tmpPropertyValue.Split('T')[0];
                    }
                    if (pattern == "long_date_time")
                    {
                        tmpPropertyValue = tmpPropertyValue.Replace("T", " ");
                    }

                    str_r += " label='" + tmpPropertyValue + "' pattern='" + pattern + "'";

                    break;
                case "list":

                    string ListName = PropertyParam;
                    if (IsInputMode)
                    {
                        //System.Diagnostics.Debugger.Break();;;
                        aml = "<AML>";
                        aml += "<Item type='Value' action='get' select='label,value' language='" + GetUserInfo(UserInfo, "lang") + "'>";
                        aml += "<source_id>";
                        aml += "<Item type='List' action='get'>";
                        aml += "<name>" + ListName + "</name>";
                        aml += "</Item>";
                        aml += "</source_id>";
                        aml += "</Item>";
                        aml += "</AML>";

                        string strListValues = "";

                        r_item = inn.applyAML(aml);
                        if (!r_item.isError())
                        {
                            //r_PropertyValue="";
                            strListValues = "";
                            for (int k = 0; k < r_item.getItemCount(); k++)
                            {
                                //value^^label@@value^^label@@......
                                Item ListItem = r_item.getItemByIndex(k);
                                //r_PropertyValue += ListItem.getProperty("value") + "^^" + GetMultilangProperty(ListItem, "label") + "@@";
                                strListValues += ListItem.getProperty("value") + "^^" + GetMultilangProperty(ListItem, "label") + "@@";
                            }
                            //r_PropertyValue = r_PropertyValue.TrimEnd('@');
                            //r_PropertyValue = "<![CDATA[" + r_PropertyValue + "]]>";
                            strListValues = strListValues.TrimEnd('@');
                            //strListValues = "<![CDATA[" + strListValues + "]]>";

                        }
                        //str_r += " label='" + tmpPropertyValue + "'";
                        str_r += " listvalues='" + strListValues + "'";
                    }
                    else
                    {
                        aml = "<AML>";
                        aml += "<Item type='Value' action='get' select='label' language='" + GetUserInfo(UserInfo, "lang") + "'>";
                        aml += "<source_id>";
                        aml += "<Item type='List' action='get'>";
                        aml += "<name>" + ListName + "</name>";
                        aml += "</Item>";
                        aml += "</source_id>";
                        aml += "<value>" + tmpPropertyValue + "</value>";
                        aml += "</Item>";
                        aml += "</AML>";
                        r_item = inn.applyAML(aml);
                        if (!r_item.isError())
                        {
                            tmpPropertyValue = GetMultilangProperty(r_item, "label");
                        }
                        str_r += " label='" + tmpPropertyValue + "'";
                        str_r += " listvalues=''";
                    }

                    break;
                case "filter list":
                    string fListName = PropertyParam;
                    aml = "<AML>";
                    aml += "<Item type='Filter Value' action='get' select='label' language='" + lang + "'>";
                    aml += "<source_id>";
                    aml += "<Item type='List' action='get'>";
                    aml += "<name>" + fListName + "</name>";
                    aml += "</Item>";
                    aml += "</source_id>";
                    aml += "<value>" + tmpPropertyValue + "</value>";
                    aml += "</Item>";
                    aml += "</AML>";
                    r_item = inn.applyAML(aml);
                    if (!r_item.isError())
                    {
                        //tmpPropertyValue = r_item.getProperty("label", "", lang);
                        tmpPropertyValue = GetMultilangProperty(r_item, "label");
                    }
                    str_r += " label='" + tmpPropertyValue + "'";
                    break;
                case "string":
                    tmpPropertyValue = oriPropertyValue;
                    break;

                default:
                    tmpPropertyValue = oriPropertyValue;
                    break;

            }

            //如果IsInputMode,要表现出必填的属性
            if (IsInputMode)
            {
                str_r += " is_required='" + ContextItem.getProperty("is_required", "0") + "'";
            }

        GoEnd:

            str_r += " otherattributes=''>";
            str_r += r_PropertyValue;
            str_r += "</" + TagPrefix + ">";

            return str_r;
        }


        public string GetUserInfo(string UserInfo, string section)
        {
            string r = "";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(UserInfo);
            XmlNode tmp = xmldoc.SelectSingleNode("//" + section);
            r = tmp.InnerText;
            return r;
        }

        public string GetMyActivity(string criteria)
        {
            //System.Diagnostics.Debugger.Break();
            string r = "";
            string aml = "";
            //string parameter = "<UserID>" + inn.getUserID() + "</UserID>";
            //parameter += "<ViewMode>Active</ViewMode>";
            //parameter += "<WorkflowTasksFlag>1</WorkflowTasksFlag>";
            //parameter += "<ProjectTasksFlag>0</ProjectTasksFlag>";
            //parameter += "<ActionTasksFlag>0</ActionTasksFlag>";
            //parameter += "<ActVotePageMapping>in_QizHeader:ActVote_Qiz;in_action:ActVote_Action</ActVotePageMapping>";

            XmlDocument xmlActVotePageMapping = new XmlDocument();
            xmlActVotePageMapping.LoadXml("<root>" + criteria + "</root>");

            string ActVotePageMapping = "";
            if (xmlActVotePageMapping.SelectSingleNode("//ActVotePageMapping") != null)
            {
                ActVotePageMapping = xmlActVotePageMapping.SelectSingleNode("//ActVotePageMapping").InnerText;
            }


            string TemplateName = "";

            Item r_Item = inn.applyMethod("In_GetAssignedTasks", criteria);

            string tmp = r_Item.dom.SelectSingleNode("//Result").InnerXml;
            //HttpUtility.HtmlDecode(r_Item.dom.SelectSingleNode("//Result").InnerXml)
            tmp = HttpUtility.HtmlDecode(tmp);

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(tmp);
            string r_MyActivity = "";
            XmlNodeList nodelist = xmldoc.SelectNodes("//tr");
            /*
            <tr id='6D8F5D47B6FA4DD680287FEE8CD3B455:4491CA71A21240438418F7B52632DCE9'>
		    <td>
			    <![CDATA[<img align='center' src='' />]]></td>
		    <td>
			    <![CDATA[<img align='center' src='../images/InBasketWorkflow.svg' />]]></td>
		    <td>
			    <![CDATA[d3]]>
		    </td>
		    <td>
			    <![CDATA[b]]>
		    </td>
		    <td>
			    <![CDATA[Active]]>
		    </td>
		    <td>
			    <![CDATA[2014-08-19T16:42:38]]>
		    </td>
		    <td>
			    <![CDATA[]]>
		    </td>
		    <td>
			    <![CDATA[Please Review...]]>
		    </td>
		    <td>
			    <![CDATA[Innovator Admin]]>
		    </td>
		    <userdata key="assignmentType" value="workflow" />
		    <userdata key="assignmentID" value="4491CA71A21240438418F7B52632DCE9" />
		    <userdata key="lockType" value="0" />
		    <userdata key="can_refuse" value="1" />
		    <userdata key="can_delegate" value="1" />
		    <userdata key="attachedId" value="1D6C6D1040A1455088C297B11B4BF9F5" />
		    <userdata key="attachedTypeId" value="B88C14B99EF449828C5D926E39EE8B89" />
		    <userdata key="attachedType" value="Document" />
		    <userdata key="workflowProcessId" value="8E9133F6DA09489D8B72B1D1996B787B" />
	    </tr>
             * */


            foreach (XmlNode xitem in nodelist)
            {
                r_MyActivity += "<Item>";
                XmlNodeList tdnodelist = xitem.SelectNodes("td");
                string workflow_name = TrimCDATA(tdnodelist[2].InnerText);
                string activity_name = TrimCDATA(tdnodelist[3].InnerText);
                string stard_date = TrimCDATA(tdnodelist[5].InnerText);

                string[] Date_Day = stard_date.Split('T');
                string Get_DateDay = Date_Day[0];

                string end_date = "";
                if (tdnodelist[6] != null)
                    end_date = TrimCDATA(tdnodelist[6].InnerText);

                XmlNodeList userdatanodelist = xitem.SelectNodes("userdata");
                string assignmentType = TrimCDATA(userdatanodelist[0].Attributes["value"].Value);
                string activity_id = "";
                string attachedId = "";
                string attachedType = "";
                string workflowProcessId = "";

                string projectId = "";
                string assignmentId = "";

                string wbsId = "";
                string strLabelPropName = "";

                if (assignmentType == "project")
                {
                    projectId = TrimCDATA(userdatanodelist[1].Attributes["value"].Value);
                    assignmentId = TrimCDATA(userdatanodelist[2].Attributes["value"].Value);
                    activity_id = TrimCDATA(userdatanodelist[4].Attributes["value"].Value);
                    wbsId = TrimCDATA(userdatanodelist[6].Attributes["value"].Value);
                    TemplateName = "in_MyActivity2";
                    strLabelPropName = workflow_name;
                }
                if (assignmentType == "workflow")
                {
                    activity_id = TrimCDATA(userdatanodelist[1].Attributes["value"].Value);
                    attachedId = TrimCDATA(userdatanodelist[5].Attributes["value"].Value);
                    attachedType = TrimCDATA(userdatanodelist[7].Attributes["value"].Value);
                    workflowProcessId = TrimCDATA(userdatanodelist[8].Attributes["value"].Value);
                    TemplateName = "in_MyActivity";
                    //2016-03-17 增加workflowProcessLabel,以便UI上可以看到中文的类型分隔
                    aml = "<AML>";
                    aml += "<Item type='Workflow Process' action='get' select='label' id='" + workflowProcessId + "' language='" + lang  + "'> ";
                    aml += "</Item></AML>";
                    Item itmWP = inn.applyAML(aml);
                    strLabelPropName = itmWP.getProperty("label", attachedType, lang);
                }


                //if(attachedType=="In_WorkflowForm")
                //    aml += "<Item type='In_WorkflowForm' action='get' select='in_itemtype_label,in_itemtype,in_wfp_label' id='" + attachedId + "'>";
                //else
                //    aml += "<Item type='ItemType' action='get' select='label'><name>" + attachedType + "</name></Item>";
                //aml += "</Item></AML>";

                //Item itmItemtype = inn.applyAML(aml);

                //if (attachedType == "In_WorkflowForm")
                //{
                //    strLabelPropName = itmItemtype.getProperty("in_itemtype_label","",lang);
                //    if(strLabelPropName=="")
                //        strLabelPropName = itmItemtype.getProperty("in_itemtype", "其他");
                //}
                //else
                //{
                //    strLabelPropName = itmItemtype.getProperty("label", attachedType,lang);
                //}

                r_MyActivity += "<workflow_name><![CDATA[" + workflow_name + "]]></workflow_name>";
                r_MyActivity += "<activity_name><![CDATA[" + activity_name + "]]></activity_name>";
                r_MyActivity += "<stard_date>" + Get_DateDay + "</stard_date>";
                r_MyActivity += "<end_date>" + end_date + "</end_date>";
                r_MyActivity += "<activity_id>" + activity_id + "</activity_id>";
                r_MyActivity += "<attachedId>" + attachedId + "</attachedId>";
                r_MyActivity += "<attachedType>" + attachedType + "</attachedType>";
                r_MyActivity += "<workflowProcessId>" + workflowProcessId + "</workflowProcessId>";
                r_MyActivity += "<in_workflowProcessLabel>" + strLabelPropName + "</in_workflowProcessLabel>";
                r_MyActivity += "<projectId>" + projectId + "</projectId>";
                r_MyActivity += "<wbsId>" + wbsId + "</wbsId>";
                r_MyActivity += "<assignmentId>" + assignmentId + "</assignmentId>";

                string TargetPage = GetParameterValue(ActVotePageMapping, attachedType, ';', ':');
                r_MyActivity += "<targetPage>" + TargetPage + "</targetPage>";

                r_MyActivity += "</Item>";
            }
            string Status = "true";
            criteria = "<request><![CDATA[" + criteria + "]]></request>";
            r = BuildResponse(Status, r_MyActivity, TemplateName, criteria);



            return r;
        }

        private string TrimCDATA(string s)
        {
            s = s.Replace("<![CDATA[", "");
            s = s.Replace("]]>", "");
            return s;
        }

        /// <summary>
        /// 組合出要丟給App Client 端的 xml,包含標準的狀態、內容、模板、原始要求資訊
        /// </summary>
        /// <param name="Status">狀態:true / false</param>
        /// <param name="Result">實際要傳遞給client端的資訊(不會處理 xml 逃脫字元)</param>
        /// <param name="TemplateName">本次內容所對應的模板form名稱(client會自動辨識是否需要抓取新版模板)</param>
        /// <param name="ExtraInfo">其他MetaDa</param>
        /// <returns></returns>
        public string BuildResponse(string Status, string Result, string TemplateName, string ExtraInfo)
        {
            //System.Diagnostics.Debugger.Break();;;
            XmlDocument xmlUserInfo = new XmlDocument();
            //xmldoc.LoadXml(decodepostContext);
            xmlUserInfo.LoadXml(UserInfo);

            string NewFormTemplateContext = "";

            string ClientTemplateNames = "";
            string NewClientTemplateNames = "";

            if (TemplateName != "")
            {
                XmlNode xmltmp = xmlUserInfo.SelectSingleNode("//clienttemplates");
                ClientTemplateNames = (xmltmp == null) ? "" : xmltmp.InnerText;
                NewClientTemplateNames = ClientTemplateNames;
                //ClientTemplateNames=""in_Act2Vote,6&in_Activity_App1,2&in_default_web_list,3&in_default_web_view,3&in_default_web_relationship,2&in_PrjTree,1&in_Project_App1,8&in_sign_off,2""
                int TemplateNameIndex = ClientTemplateNames.IndexOf(TemplateName);

                string[] ClientTemplateNamesArr = ClientTemplateNames.Split('&');
                string ClientTemplateVer = "-1";
                for (int i = 0; i < ClientTemplateNamesArr.Length; i++)
                {
                    string[] ClientTemplateName = ClientTemplateNamesArr[i].Split(',');
                    if (ClientTemplateName[0] == TemplateName)
                    {
                        ClientTemplateVer = ClientTemplateName[1];
                        break;
                    }
                }

                string aml = "";
                aml = "<AML>";
                aml += "<Item type='Form' action='get'>";
                aml += "<name>" + TemplateName + "</name>";
                aml += "</Item>";
                aml += "</AML>";

                Item TemplateForm = inn.applyAML(aml);
                if (TemplateForm.isError())
                {
                    throw new Exception(TemplateForm.getErrorString());
                }

                string CurTemplateVer = TemplateForm.getProperty("in_useweb_ver", "0");
                if (CurTemplateVer != ClientTemplateVer)
                {
                    //代表 Server 端 Template 有进版
                    NewFormTemplateContext = GetTemplate(TemplateName);
                    if (ClientTemplateVer == "-1")
                    {
                        NewClientTemplateNames += "&" + TemplateName + "," + CurTemplateVer;
                    }
                    else
                    {
                        NewClientTemplateNames = NewClientTemplateNames.Replace(TemplateName + "," + ClientTemplateVer, TemplateName + "," + CurTemplateVer);
                    }
                }

            }

            string r = "";
            r = "<response>";
            r += "<status>" + Status + "</status>";
            r += "<meta>";
            r += "<template_name>" + TemplateName + "</template_name>";
            r += "<templatenames><![CDATA[" + NewClientTemplateNames + "]]></templatenames>";
            r += NewFormTemplateContext;
            r += ExtraInfo;
            r += "</meta>";
            r += "<result>";
            r += Result;
            r += "</result>";
            r += "</response>";

            return r;
        }

        public string GetSignOff(string ProcessID)
        {
            //System.Diagnostics.Debugger.Break();;;
            string r = "";
            string aml = "";
            aml += "<AML>";
            aml += "<Item type='Workflow Process' id='" + ProcessID + "' initial_action='get' action='do_l10n' select='active_date, closed_date, state, name, description, created_by_id,sub_of' >";
            aml += "  <created_by_id>";
            aml += "    <Item type='User' select='first_name,last_name' action='get'>";
            aml += "      <Relationships>";
            aml += "        <Item type='Alias' action='get' select='related_id(name)'/>";
            aml += "      </Relationships>";
            aml += "    </Item>";
            aml += "  </created_by_id>";
            aml += "  <Relationships>";
            aml += "    <Item type='Workflow Process Activity' action='get' select='related_id'>";
            aml += "      <related_id>";
            aml += "      <Item type='Activity' action='get' select='label,name,state,active_date,subflow(name)'>";
            aml += "        <and>";
            aml += "          <state condition='ne'>Pending</state>";
            aml += "          <or>";
            aml += "              <is_start condition='ne'>1</is_start>";
            aml += "              <is_auto condition='ne'>1</is_auto>";
            aml += "          </or>";
            aml += "          <or>";
            aml += "              <is_end condition='ne'>1</is_end>";
            aml += "              <is_auto condition='ne'>1</is_auto>";
            aml += "          </or>";
            aml += "        </and>";
            aml += "				<Relationships>";
            aml += "					<Item type='Activity Assignment' action='get' select='closed_by, is_disabled, closed_on, comments, path, related_id(name)'>";
            aml += "            <closed_by>";
            aml += "              <Item type='User' select='first_name,last_name' action='get'>";
            aml += "                <Relationships>";
            aml += "                  <Item type='Alias' action='get' select='related_id(name)'/>";
            aml += "                </Relationships>";
            aml += "              </Item>";
            aml += "            </closed_by>";
            aml += "          </Item>   ";
            aml += "        </Relationships>";
            aml += "      </Item>";
            aml += "      </related_id>";
            aml += "    </Item>";
            aml += "  </Relationships>";
            aml += "</Item>' ";
            aml += "</AML>";

            Item SignOff = inn.applyAML(aml);

           
            string status = "false";
            string str_r = "";

            if (SignOff.isError())
            {
                status = "false";
                str_r = SignOff.getErrorString();
            }
            else
            {
                status = "true";

                str_r = "<Header>";
                str_r += "<p1_label>Activity</p1_label>";
                str_r += "<p2_label>State</p2_label>";
                str_r += "<p3_label>Assigned To</p3_label>";
                str_r += "<p4_label>Completed By</p4_label>";
                str_r += "<p5_label>How Voted</p5_label>";
                str_r += "<p6_label>When</p6_label>";
                str_r += "<p7_label>Comments</p7_label>";
                str_r += "</Header>";

                Item WfpActs = SignOff.getRelationships("Workflow Process Activity");

                int WfpActs_Count = WfpActs.getItemCount();
                string CorrectOrderAAIDs = "";
                XmlNodeList AAs = WfpActs.dom.SelectNodes("//Item[@type='Activity Assignment']/id");
                foreach (XmlNode x in AAs)
                {
                    CorrectOrderAAIDs += "'" + x.InnerText + "',";
                }
                CorrectOrderAAIDs = CorrectOrderAAIDs.TrimEnd(',');

                aml = "<AML>";
                aml += "<Item type='Activity Assignment' action='get' where=\"[Activity_Assignment].[id] in (" + CorrectOrderAAIDs + ")\" orderBy='closed_on' select='source_id(state,label,name),closed_by, is_disabled, closed_on, comments, path, related_id(name)' language='" + lang + "'>";
                aml += "</Item>";
                aml += "</AML>";

                Item tmp = inn.applyAML(aml);
                CorrectOrderAAIDs = "";
                if (!tmp.isError())
                {
                    for (int i = 1; i < tmp.getItemCount(); i++)
                    {
                        CorrectOrderAAIDs += tmp.getItemByIndex(i).getID() + ",";
                    }
                }
                CorrectOrderAAIDs += tmp.getItemByIndex(0).getID(); //因为 Closed_on 空白的要放最后一个
                string[] arrCorrectOrderAAIDs = CorrectOrderAAIDs.Split(',');
                for (int i = 0; i < tmp.getItemCount(); i++)
                {
                    Item tmpAA = tmp.getItemsByXPath("//Item[@id='" + arrCorrectOrderAAIDs[i] + "']");
                    string state = tmpAA.getPropertyItem("source_id").getProperty("state", "");
                    //Item tmpAA = tmpWfpAct.getRelatedItem().getRelationships("Activity Assignment").getItemByIndex(0);
                    string When = tmpAA.getProperty("closed_on", "");
                    if (state == "Closed" && When == "")
                    {
                        //代表这是自动节点
                        continue;
                    }

                    str_r += "<Item>";
                    //str_r += "<p1_value>" + tmpAA.getPropertyItem("source_id").getProperty("label", "", lang) +"</p1_value>";
                    str_r += "<p1_value>" + GetMultilangProperty(tmpAA.getPropertyItem("source_id"), "label") + "</p1_value>";
                    str_r += "<p2_value>" + state + "</p2_value>";
                    str_r += FormatValue(tmpAA.getItemByIndex(0).getRelatedItem(), "id", "item", "Identity", "p3_value", "");


                    //string Completed_By = (tmpAA.getPropertyItem("closed_by") == null) ? "" : tmpAA.getPropertyItem("closed_by").getPropertyAttribute("id", "keyed_name");
                    //string Completed_By =FormatValue(tmpAA.getPropertyItem("closed_by"),"id","item","User","p4_value");
                    string How_Voted = tmpAA.getProperty("path", "");
                    string Comments = tmpAA.getProperty("comments", "");

                    //str_r += "<p4_value>" + Completed_By + "</p4_value>";
                    str_r += FormatValue(tmpAA, "closed_by", "item", "User", "p4_value", "");
                    str_r += "<p5_value>" + How_Voted + "</p5_value>";
                    str_r += FormatValue(tmpAA, "closed_on", "date", "short_date_time", "p6_value", "");
                    str_r += "<p7_value><![CDATA[" + Comments + "]]></p7_value>";
                    str_r += "</Item>";
                }



            }
            ProcessID = "<request><![CDATA[" + ProcessID + "]]></request>";
            r = BuildResponse(status, str_r, "in_sign_off", ProcessID);

            return r;
        }

        public bool IsPloyItem(string ItemName)
        {
            if (PolyItems == "")
                GetPolyItems();
            bool r = false;
            ItemName = "," + ItemName + ",";
            if (PolyItems.IndexOf(ItemName) > -1)
                r = true;
            return r;
        }

        private void GetPolyItems()
        {
            //取得 Polyitems
            string aml = "<AML>";
            aml += "<Item type='ItemType' action='get' select='name'>";
            aml += "<implementation_type>polymorphic</implementation_type>";
            aml += "</Item>";
            aml += "</AML>";

            Item r = inn.applyAML(aml);
            PolyItems = ",";
            for (int i = 0; i < r.getItemCount(); i++)
            {
                PolyItems += r.getItemByIndex(i).getProperty("name", "") + ",";
            }
        }

        public string Getl10nValue(string ui_resource_name, string key, string DefaultValue)
        {
            //System.Diagnostics.Debugger.Break();;;
            //取得 ui_resource
            string str_r = DefaultValue;
            if (xmlui_resource.DocumentElement == null)
            {

                string aml = "<AML>";
                aml += "<Item type='Form' action='get' where=\"[Form].[name]='" + ui_resource_name + "'\">";
                aml += "<Relationships>";
                aml += "<Item type='Body' action='get' select='id'>";
                aml += "<Relationships>";
                aml += "<Item type='Field' action='get' select='name,html_code' where=\"[Field].[name] in ('in_template','in_parameter','in_template_header')\" />";
                aml += "</Relationships>";
                aml += "</Item>";
                aml += "</Relationships>";
                aml += "</Item>";
                aml += "</AML>";

                Item r_item = inn.applyAML(aml);



                if (!r_item.isError())
                {
                    Item tmp_Fields = r_item.getRelationships("Body").getItemByIndex(0).getRelationships("Field");
                    for (int m = 0; m < tmp_Fields.getItemCount(); m++)
                    {
                        Item tmp_Field = tmp_Fields.getItemByIndex(m);
                        if (tmp_Field.getProperty("name") == "in_template")
                        {
                            xmlui_resource.LoadXml(tmp_Field.getProperty("html_code", ""));
                        }
                    }
                }

            }

            if (xmlui_resource != null)
            {
                XmlNode tmp = xmlui_resource.SelectSingleNode("//resource[@key='" + key + "']/" + lang);
                if (tmp == null)
                {
                    str_r = DefaultValue;
                }
                else
                {
                    str_r = tmp.InnerText;
                }
            }

            return str_r;


        }

        public Item GetCurrentItemById(string ItemType, string ItemId)
        {
            //return _InnH.GetCurrentItemById( ItemType,  ItemId);
            //百聿專屬
            string aml = "";
            //改成取最新generation 的Item
            aml = "<AML>";
            aml += "<Item type='" + ItemType + "'  action='get' select='config_id' id='" + ItemId + "'>";
            aml += "</Item>";
            aml += "</AML>";
            Item item_r = inn.applyAML(aml);

            item_r = GetCurrentItemByConfigId(ItemType, item_r.getProperty("config_id"));
            return item_r;
        }

        public Item GetCurrentItemByConfigId(string ItemType, string ConfigId)
        {
            //return _InnH.GetCurrentItemByConfigId(ItemType, ConfigId);
            string aml = "";
            //改成取最新generation 的Item
            aml = "<AML>";
            aml += "<Item type='" + ItemType + "'  action='get' select='config_id'>";
            aml += "<config_id>" + ConfigId + "</config_id>";
            aml += "<is_current>1</is_current>";
            aml += "</Item>";
            aml += "</AML>";
            Item item_r = inn.applyAML(aml);
            if (item_r.isError())
            {
                //str_r = item_r.getErrorString();
                //goto End;
            }
            item_r = item_r.getItemByIndex(0);
            return item_r;
        }

        /// <summary>
        /// UnEscape client 傳過來的 content,^amp變成&, ^co變成: 
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public string UnEscapeInnosoftChar(string content)
        {
            string r = content;
            r = r.Replace("^amp", "&");
            r = r.Replace("^co", ":");
            return r;
        }


        public  double GetDistance(string pos1, string pos2)
        {

            double lat1, lng1, lat2, lng2;
            lat1 = Double.Parse(pos1.Split(',')[0].Trim());
            lng1 = Double.Parse(pos1.Split(',')[1].Trim());
            lat2 = Double.Parse(pos2.Split(',')[0].Trim());
            lng2 = Double.Parse(pos1.Split(',')[1].Trim());


            double dis = 0;
            var radLat1 = toRadians(lat1);
            var radLat2 = toRadians(lat2);
            var deltaLat = radLat1 - radLat2;
            var deltaLng = toRadians(lng1) - toRadians(lng2);
            dis = 2 * Math.Asin(Math.Sqrt(Math.Pow(Math.Sin(deltaLat / 2), 2) + Math.Cos(radLat1) * Math.Cos(radLat2) * Math.Pow(Math.Sin(deltaLng / 2), 2)));
            return dis * 6378137;
        }

        private  double toRadians(double d) { return d * Math.PI / 180; }


    }
}

